package fr.ajc.spring.gabriel.tp;

import java.time.LocalDateTime;



import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import fr.ajc.spring.gabriel.tp.model.Saison;



@Controller

public class SimpleController {

	@Value("${spring.application.name}")
	String appName;

	@GetMapping("/")
	public String homePage(Model model) {
		model.addAttribute("appName", appName);
		return "home";
	}

	@GetMapping("/date_heure")
	public String date_heurePage(Model model) {



		LocalDateTime now = LocalDateTime.now();

		model.addAttribute("localDateTime", now);
		LocalDateTime dateTime = LocalDateTime.of(2019, Month.APRIL, 8, 12, 30);


		int month = dateTime.getMonth().getValue();
		int day = dateTime.getDayOfMonth();

		if ((month == 12 && day >= 16 && day <= 31) || (month == 1 && day >= 1 && day <= 31)
				|| (month == 2 && day >= 1 && day <= 28) || (month == 3 && day >= 1 && day <= 15)) {
			model.addAttribute("saison", "hiver");
		} else if ((month == 3 && day >= 16 && day <= 31) || (month == 4 && day >= 1 && day <= 30)
				|| (month == 5 && day >= 1 && day <= 31) || (month == 6 && day >= 1 && day <= 15)) {
			model.addAttribute("saison", "printemps");
		} else if ((month == 6 && day >= 16 && day <= 30) || (month == 7 && day >= 1 && day <= 31)
				|| (month == 8 && day >= 1 && day <= 31) || (month == 9 && day >= 1 && day <= 15)) {
			model.addAttribute("saison", "ete");
		} else if ((month == 9 && day >= 16 && day <= 30) || (month == 10 && day >= 1 && day <= 31)
				|| (month == 11 && day >= 1 && day <= 30) || (month == 12 && day >= 1 && day <= 15)) {
			model.addAttribute("saison", "automne");
		}
		return "date_heure";
	}

	@GetMapping("/saison")
	public String testsaison(Model model) {
		
		LocalDateTime now = LocalDateTime.now();
		
		model.addAttribute("localDateTime", now);


		Saison a = new Saison("hiver",false);
		Saison b = new Saison("printemps",false);
		Saison c = new Saison("ete",false);
		Saison d = new Saison("automne",false);

		List<Saison> annee = new ArrayList();

		annee.add (a);
		annee.add (b);
		annee.add (c);
		annee.add (d);
		
		
		int month = now.getMonth().getValue();
		int day = now.getDayOfMonth();
		

		if ((month == 12 && day >= 16 && day <= 31) || (month == 1 && day >= 1 && day <= 31) || (month == 2 && day >= 1 && day <= 28) || (month == 3 && day >= 1 && day <= 15)) {
			a.setCurrent(true)	;
			model.addAttribute( "saison", "hiver");
			
		} else if ((month == 3 && day >= 16 && day <= 31) || (month == 4 && day >= 1 && day <= 30) || (month == 5 && day >= 1 && day <= 31) || (month == 6 && day >= 1 && day <= 15)) {
			b.setCurrent(true)	;	
			model.addAttribute("saison","printemps");
			
		} else if ((month == 6 && day >= 16 && day <= 30) || (month == 7 && day >= 1 && day <= 31) || (month == 8 && day >= 1 && day <= 31) || (month == 9 && day >= 1 && day <= 15)) {
			c.setCurrent(true)	;	
			model.addAttribute("saison","ete" );
			
		} else if ((month == 9 && day >= 16 && day <= 30) || (month == 10 && day >= 1 && day <= 31) || (month == 11 && day >= 1 && day <= 30) || (month == 12 && day >= 1 && day <= 15)) {
			d.setCurrent(true)	;	
			model.addAttribute("saison","automne");
		}
		model.addAttribute("annee", annee);
		
		return "saison";

		}

	

	
	
	
	}
	
	
	








